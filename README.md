# Wardrobify

Team:

* Spencer Rhyne - Hats Microservice
* Greg Robertson - Shoes Microservice

## Design
wardrobe will have the children of shoes and hats microservices
shoes and hats will have a one to many relationship with the wardrobe

## Shoes microservice
Shoe will track its manufacturer, its model name, its color, a URL for a picture, and the bin in the wardrobe where it exists.

## Hats microservice
Have hats model that has fields for: fabric, style_name, color, picture_url, location where it exists.

I need to grab the data that I need from the RESTful API so that I can use it in my hats microservices for the location and the bin. 

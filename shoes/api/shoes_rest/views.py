from .models import Shoe, BinVO
from django.http import JsonResponse
import json
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder


class BinVODetailEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "closet_name",
        "bin_number",
        "bin_size",
    ]
class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "id",
        "manufacturer",
        "model_name",
        "color",
        "picture_url",
        ]
    def get_extra_data(self, o):
        return {"bin": o.bin.closet_name}
class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "manufacturer",
        "model_name",
        "color",
        "picture_url",
        "bin",
    ]
    encoders = {
        "bin": BinVODetailEncoder(),
    }

@require_http_methods(["GET", "POST"])#-------------------allow only GET and POST requests to this method
def api_list_shoes(request, bin_vo_id=None):
    if request.method == "GET":#--------------if block for the existing code to only run if the HTTP method is GET.
        if bin_vo_id is not None:
           shoes = Shoe.objects.filter(bin=bin_vo_id) 
        else:
            shoes = Shoe.objects.all()#----------Query Set
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeListEncoder,
    )
    else: #---------------------------------------The else block will handle the POST.
        content = json.loads(request.body) #------- json.loads converts the JSON-formatted string stored in request.body.
        print(content)
        
        try:
            bin_href = content["bin"]
            bin = BinVO.objects.get(import_href=bin_href)
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin id"},
                status=400,
            )

        shoes = Shoe.objects.create(**content)
        return JsonResponse(
            shoes,
            encoder=ShoeDetailEncoder,
            safe=False,
        )

@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_shoe(request, pk):
    if request.method == "GET":
        shoe = Shoe.objects.get(id=pk)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Shoe.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        try:
            if "bin" in content:
                bin_href = content["bin"]
                bin = BinVO.objects.get(import_href=bin_href)
                content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"},
                status=400,
            )
        Shoe.objects.filter(id=pk).update(**content)
        location = Shoe.objects.get(id=pk)
        return JsonResponse(
            location,
            encoder=ShoeDetailEncoder,
            safe=False,
        )    

# class BinVODetailEncoder(ModelEncoder):
#     model = BinVO
#     properties = [
#         "import_href",
#         "closet_name",
#         "bin_number",
#         "bin_size",
#     ]
# class ShoeListEncoder(ModelEncoder):
#     model = Shoe
#     properties = [
#         "id",
#         "manufacturer",
#         "model_name",
#         "color",
#         "picture_url",
#         ]
#     encoders = {
#     "bin": BinVODetailEncoder(),
# }
#     def get_extra_data(self, o):
#         return {"bin": o.bin.closet_name}

# @require_http_methods(["GET", "POST"])#-------------------allow only GET and POST requests to this method
# def api_list_shoes(request):
#     if request.method == "GET":#--------------if block for the existing code to only run if the HTTP method is GET.
#         shoes = Shoe.objects.all()#----------Query Set
#         return JsonResponse(
#             shoes,
#             encoder=ShoeListEncoder,
#             safe=False
#     )
#     else: #---------------------------------------The else block will handle the POST.
#         content = json.loads(request.body) #------- json.loads converts the JSON-formatted string stored in request.body.
#         print(content)
        
#         try:
#             bin_href = content["bin"]
#             bin = BinVO.objects.get(import_href=bin_href)
#             content["bin"] = bin
#         except BinVO.DoesNotExist:
#             return JsonResponse(
#                 {"message": "Invalid bin id"},
#                 status=400,
#             )

#         shoes = Shoe.objects.create(**content)
#         return JsonResponse(
#             shoes,
#             encoder=ShoeListEncoder,
#             safe=False,
#         )
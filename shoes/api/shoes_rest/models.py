from django.db import models

class BinVO(models.Model):
    import_href = models.CharField(max_length=200,unique=True, null=True, blank=True)
    closet_name = models.CharField(max_length=100)
    bin_number = models.PositiveSmallIntegerField()
    bin_size = models.PositiveSmallIntegerField()

class Shoe(models.Model):
    manufacturer = models.CharField(max_length=200)
    model_name = models.CharField(max_length=200)
    color = models.CharField(max_length=200)
    picture_url = models.URLField(null=True)
    # OneToMany Relationship 
    bin = models.ForeignKey(
        BinVO,
        related_name="shoes", #(related name, takes on the plural of the {reverse relation} / Shoe Model)
        on_delete=models.CASCADE,
    )
    
    
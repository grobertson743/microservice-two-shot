import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ShoeForm from './ShoeForm';
import ShoeList from './ShoeList';
import HatsList from './HatsList';
import CreateHatForm from './CreateHatForm';

function App(props) {
  if (props.shoes === undefined && props.hats === undefined) {
    return null
  }

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="shoes">
            <Route path="new" element={<ShoeForm />} />
            <Route path="" element={<ShoeList shoes={props.shoes} />} />
          </Route>
          <Route path="hats">
            <Route index element={<HatsList hats={props.hats} />} />
            <Route path="new" element={<CreateHatForm />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;

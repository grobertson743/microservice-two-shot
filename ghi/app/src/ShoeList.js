import React from "react";





function ShoeList(props) {

    async function DeleteShoe(id) {
        const deleteUrl = `http://localhost:8080/api/shoes/${id}/`
        const fetchConfig = {
            method: "delete"
        }
        const response = await fetch(deleteUrl, fetchConfig)
        if (response.ok) {
            console.log('Delete Sucessfull', response)
        }
    }

    return (
        <table className=' table table-striped '>
            <thead>
                <tr>
                    <th>Picture</th>
                    <th>Manufacturer</th>
                    <th>Model Name</th>
                    <th>Color</th>
                    <th>Bin</th>
                    <th>Delete</th>
                </tr>
            </thead>
            <tbody>

                {props.shoes.map(shoe => {
                    return (
                        <tr key={shoe.id}>
                            <td><img src={shoe.picture_url} alt='' width="20%" height="20%" /></td>
                            <td td > {shoe.manufacturer}</td >
                            <td>{shoe.model_name}</td>
                            <td>{shoe.color}</td>
                            <td>{shoe.bin}</td>
                            <td> <button onClick={() => DeleteShoe(shoe.id)} type="button" className='btn btn-dark'>Delete</button>
                            </td>

                        </tr >
                    );
                })}
            </tbody >
        </table >
    )
}

export default ShoeList

